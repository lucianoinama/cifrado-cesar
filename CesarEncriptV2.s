.data
@############################################################ SECCION DATOS DEL MENU #####################################################################
line1: .asciz "_________________________________________________________     \\\|||@/"
line1Length = . - line1
line2: .asciz "| CCCCCCCCCC  EEEEEEEE   SSSSSSS     AA      RRRRRR    |  .  ======="
line2Length = . - line2

line3: .asciz "| CC          EE        SS          AAAA     RR   RR   |  / \| O   O |"
line3Length = . - line3

line4: .asciz "| CC          EEEEEEEE   SSSSS     AA  AA    RRRRRR    |  \ / \`  _ '/"
line4Length = . - line4

line5: .asciz "| CC          EE              SS  AA AA AA   RR  RR    |   #   | |"
line5Length = . -line5

line6: .asciz "| CCCCCCCCCC  EEEEEEEE  SSSSSSS  AA      AA  RR   RR   |  (# (     )  "
line6Length = . - line6

line7: .asciz "| ______________________________________  ENCRIPT _____    #\@|* *|\\  | "
line7Length = . - line7

line8: .asciz "                                                           #\/(  *  )/  "
line8Length = . - line8

line9: .asciz "                                                           #  =====   "
line9Length = . - line9

line10:.asciz "                  Cesar Encript V1.1                       #  (   )   "
line10Length = . - line10

line11:.asciz "       TP FINAL ORGANIZACION DEL COMPUTADOR ARM            #  || ||"
line11Length = . - line11

line12:.asciz "        Alumnos Matias Gasañol - Agustin Inama            .#---'| |`----. "
line12Length = . - line12

line13:.asciz "                                                                           "
line13Length = . - line13

line14:.asciz "[1] Codificador/Decodificador                           "
line14Length = . - line14

line15:.asciz "[2] Decodificar con pista                               "
line15Length = . - line15

line16:.asciz "[3] Salir                                               "
line16Length = . - line16

salto: .ascii "\n"
saltoLength = . - salto

err1: .ascii  "### Opcion ingresada no valida, ingrese 1 o 2 ###"
opcionesMenuIngreso: .asciz "                "
longitudIngresoOpcion = . -opcionesMenuIngreso
welcome: .asciz "Ingrese una opcion numerica y pulse enter..."
welcomeLength = . - welcome
@############################################################ FIN SECCION DATOS DEL MENU ##################################################################

@############################################################ SECCION DATOS DE CESAR 1  ###################################################################
mensaje: .ascii "Ingrese una letra por favor: "
longitudMensaje = . - mensaje
outputResultadoCesar1: .asciz "Resultado codificado: "
outputCantidadCaracteresCesar1: .asciz "Cantidad de caracteres procesados: "
mensaje2: .ascii "Ud. ha ingresado: "
longitudMensaje2 = . - mensaje2

enter: .ascii "\n"

inputUsuario:  .asciz "                                                                                                   " @por defecto puede ser arriba w
longitudInputUsuario = . - inputUsuario

@@variables reservadas para el programa
mensajeExtraido: .asciz "                                                                                                 "
longitudmensajeExtraido = . - mensajeExtraido
claveExtraida: .asciz  "                                                                                                  "

modoExtraido: .asciz " "

indicacionesUsoCesar1: .asciz ">>>Por favor ingrese mensaje, valor de corrimiento y modo de trabajo para empezar."
formatoCesar1: .asciz ">>> El formato de ingreso es: mensaje;valor;modo; ,siendo modo= c para codificar, y d para decodificar."
ejemploCesar1: .asciz ">>> mensaje a codificar;3;c; ,el valor de corrimiento debe ser entre 1-27 "
ejemploCesar1Length = . - ejemploCesar1
msjInterrupt: .asciz ">>>Alguno de los caracteres igresados no cumple con el formato, para mensaje utilizar solo letras minusculas o mayusculas entre la a-z , o A-Z"
msjInterruptLength = . -msjInterrupt

msjInterrupt2: .asciz ">>>El valor de corrimiento debe ser un valor numerico entero entre 1-27"
msjInterrupt2Length = . - msjInterrupt2

msjInterrupt3: .asciz ">>>El modo de trabajo solo puede ser c (codificar) o d (decodificar)"
msjInterrupt3Length = . - msjInterrupt3

caracteres_procesados: .asciz " "
longitudcaracteres = . -caracteres_procesados

paridad: .asciz " "
longitudparidad = . - paridad
@############################################################ FIN SECCION DATOS DE CESAR 1 ###############################################################

@############################################################# SECCION DATOS DE CESAR2      ###############################################################
textoEncriptado: .asciz "mnkuh oxgmbetwhk ftkd mptbg"
pista: .asciz "mark"

ingresoUsuarioInput:  .asciz "                                                                                                   " @por defecto puede ser arriba w
longitudInputUsuario = . - ingresoUsuarioInput

mensajeEncriptado: .asciz "                                                                                                 "
longitudmensajeExtraido = . - mensajeEncriptado
pistaExtraida: .asciz  "                                                                                                  "


outputCesar2: .asciz "resultado decodificado: "
outputCantidadCatacteresProcesadosCesar2: .asciz "Cantidad de caracteres procesados: "
outputValorCorrimiento: .asciz "Valor de corrimiento detectado: "
outputValorCorrimientolength = . - outputValorCorrimiento
noCoincidencias: .asciz "Lo siento, no se encontraron coincidencias, reiniciando programa..."
noCoincidenciasLength = .-noCoincidencias

instruccionesCesar2: .asciz "Cesar: ingrese el mensaje codificado, y la pista suministrada para decodificar "
instruccionesCesar2Length =. - instruccionesCesar2
ejemploCesar2: .asciz "Cesar: el ingreso debe ser en el siguiente formato:     mensaje;pista;    "
ejemploCesar2Length = . - ejemploCesar2


@############################################################# FIN SECCION DATOS DE CESAR2  ###############################################################

.text
presentacion:
    .fnstart
      /*Parametros pasados*/
      /*r1=puntero al string que queremos imprimir*/
      /*r2=longitud de lo que queremos imprimir*/
      mov r7, #4 /*escritura*/
      mov r0, #1 /*stdout*/
      swi 0
      bx lr /*retorno */
    .fnend

leerMensaje:
.fnstart
    @Parametros inputs: no tiene
    @Parametros output:
    @r0=char leido

    mov r7, #3    @ Lectura x teclado
    mov r0, #0      @ Ingreso de cadena

    ldr r2, =longitudIngresoOpcion @ Leer # caracteres
    ldr r1, =opcionesMenuIngreso @ donde se guarda la cadena ingresada
    swi 0        @ SWI, Software interrup
    ldr r0, [r1] @guardo en r0 el puntero a memoria del string ingresado. 1 o 2

    bx lr @volvemos a donde nos llamaron
.fnend

validarOpciones1:
ldrb r9,[r1]
cmp r9,#49
bne validarOpciones2
b cesar1                    @Si la opcion ingresada fue 1, salto a modo cesar1.

salir:
mov r7,#1
swi 0

limpiarEtiqueta2:
 eor r3,r3
 ldr r1, =mensajeEncriptado
 etiqueta_limpiar2:
       ldrb r2,[r1]
       cmp r2,#0 /*termino la frase*/
       beq  ya_limpie
       mov r3,#32   @cargo caracter pero desplazado
       strb r3,[r1]  @guardo en memoria el caracter
       add r1,#1
       b etiqueta_limpiar

ya_limpie2:
bx lr

limpiarEtiquetaValorCorrimiento2:
 eor r3,r3
 ldr r1, =caracteres_procesados
 etiqueta_limpiar_valor2:
       ldrb r2,[r1]
       cmp r2,#0 /*termino la frase*/
       beq  ya_limpie_valor
       mov r3,#32   @cargo caracter pero desplazado
       strb r3,[r1]  @guardo en memoria el caracter
       add r1,#1
       b etiqueta_limpiar_valor2

ya_limpie_valor2:
bx lr


limpiarEtiqueta:
 eor r3,r3
 ldr r1, =mensajeExtraido
 etiqueta_limpiar:
       ldrb r2,[r1]
       cmp r2,#0 /*termino la frase*/
       beq  ya_limpie
       mov r3,#32   @cargo caracter pero desplazado
       strb r3,[r1]  @guardo en memoria el caracter
       add r1,#1
       b etiqueta_limpiar

ya_limpie:
bx lr

limpiarEtiquetaValorCorrimiento:
 eor r3,r3
 ldr r1, =caracteres_procesados
 etiqueta_limpiar_valor:
       ldrb r2,[r1]
       cmp r2,#0 /*termino la frase*/
       beq  ya_limpie_valor
       mov r3,#32   @cargo caracter pero desplazado
       strb r3,[r1]  @guardo en memoria el caracter
       add r1,#1
       b etiqueta_limpiar_valor

ya_limpie_valor:
bx lr

validarOpciones2:
cmp r9,#50
bne validarOpcionErr
b cesar2

validarOpcionErr:

cmp r9,#51
beq salir
ldr r1,=err1
mov r2,#49
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=line13
mov r2,#56
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

b options
@----------------------------------------------------------
.global main

main:

ldr r1,=line1
ldr r2,=line1Length
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=line2
ldr r2,=line2Length
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=line3
ldr r2,=line3Length
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=line4
ldr r2,=line4Length
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=line5
ldr r2,=line5Length
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=line6
ldr r2,=line6Length
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=line7
ldr r2,=line7Length
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=line8
ldr r2,=line8Length
bl presentacion

ldr r1,=salto
mov r2,#2

bl presentacion
ldr r1,=line9
ldr r2,=line9Length
bl presentacion
ldr r1,=salto
mov r2,#2
bl presentacion
ldr r1,=line10
ldr r2,=line10Length
bl presentacion
ldr r1,=salto
mov r2,#2
bl presentacion
ldr r1,=line11
ldr r2,=line11Length
bl presentacion
ldr r1,=salto
mov r2,#2
bl presentacion
ldr r1,=line12
ldr r2,=line12Length
bl presentacion
ldr r1,=salto
mov r2,#2
bl presentacion
ldr r1,=line13
ldr r2,=line13Length
bl presentacion
ldr r1,=salto
mov r2,#2
bl presentacion

@presenta opciones
options:

bl limpiarEtiqueta
bl limpiarEtiquetaValorCorrimiento
bl limpiarEtiqueta2

ldr r1,=line13
ldr r2,=line13Length
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=welcome
ldr r2,=welcomeLength
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=line13
ldr r2,=line13Length
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=line14
ldr r2,=line14Length
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=line15
ldr r2,=line15Length
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=line16
ldr r2,=line16Length
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion
bl leerMensaje  @lee mensaje ingresado




bl validarOpciones1
@si el programa pasa de aca, ya tiene guardada la opcion 1 o 2 unicamente.

@si se eligió 1:




@################################################################################## CESAR 1 #################################################################
@Leo el teclado y guardo en la memoria el mensaje ingresado.
@modo de uso: echo "hola como estas" | ./leerMensaje > out.txt ; cat out.txt
@----------------------------------------------------------


@----------------------------------------------------------
leerMensajeCesar1:
.fnstart
    @Parametros inputs: no tiene
    @Parametros output:
    @r0=char leido

    mov r7, #3    @ Lectura x teclado
    mov r0, #0      @ Ingreso de cadena

    ldr r2, =longitudInputUsuario @ Leer # caracteres
    ldr r1, =inputUsuario @ donde se guarda la cadena ingresada
    ldr r11,=mensajeExtraido
    ldr r4, =claveExtraida
    ldr r12, =modoExtraido
    swi 0        @ SWI, Software interrup
   @ ldr r0, [r1]

    bx lr @volvemos a donde nos llamaron
.fnend
@----------------------------------------------------------
imprimirString:
      .fnstart
      @Parametros inputs:
      @r1=puntero al string que queremos imprimir
      @r2=longitud de lo que queremos imprimir
      mov r7, #4 @ Salida por pantalla
      mov r0, #1      @ Indicamos a SWI que sera una cadena
      swi 0    @ SWI, Software interrup
      bx lr @salimos de la funcion mifuncion
      .fnend
@----------------------------------------------------------
newLine:
      .fnstart
      push {lr}
      mov r2, #1 @Tamaño de la cadena
      ldr r1, =enter   @Cargamos en r1 la direccion del mensaje
      bl imprimirString
      pop {lr}
      bx lr @salimos de la funcion mifuncion
      .fnend
@----------------------------------------------------------

remplazo:
.fnstart
        mov r3,#0
        eor r9,r9
        eor r10,r10
        eor r8,r8
        mov r2,#0

@agregar validacion para  r6



 palabra1:
       ldrb r6,[r1] @@cargo el byte de la posicion 1, valor guardado en r8
       ldrb r5,[r11]
       cmp r6,#0 @/termino la frase/
       beq  ya_remplace
        cmp r6,#';'
        beq etiqueta2 @@si es not equal, se codifica
             push {lr}
         cmp r6, #0x20
        bleq hay_espacio
        push {lr}
       bl validarChar
       pop {lr}

        pop {lr}
        strb r6,[r11]  @@guardo en r3 el byte
        add r1,#1
        add r9, #1 @CONTADOR DE CARACTERES PROCESADOS
        add r11,#1
        b palabra1
  hay_espacio:
      sub r9, #1 @RESTA 1 AL CONTADOR SI HAY UN ESPACIO
      bx lr
  palabra2:
       ldrb r6,[r1] @@cargo el byte de la posicion 1, valor guardado en r8
       ldrb r2,[r4]
       cmp r6,#0 @/termino la frase/
       beq  ya_remplace
        cmp r6,#';'
        beq etiqueta3 @@si es not equal, se codifica
        push {lr}
        bl validarCorrimiento
        pop {lr}
        strb r6,[r4]  @@guardo en r3 el byte
        add r1,#1
        add r4,#1
        b palabra2

  palabra3:
       ldrb r6,[r1] @@cargo el byte de la posicion 1, valor guardado en r8
       ldrb r4,[r12]
       cmp r6,#0 @/termino la frase/
       beq  ya_remplace
        cmp r6,#';'
        beq etiqueta4 @@si es not equal, se codifica
       push {lr}
      bl validarModo
      pop {lr}
        strb r6,[r12]  @@guardo en r3 el byte
        add r1,#1
        add r12,#1
        b palabra3

  palabra4:
       ldrb r6,[r1] @@cargo el byte de la posicion 1, valor guardado en r8
       cmp r6,#0 @/termino la frase/
       beq  ya_remplace

  etiqueta2:
        add r1,#1
        strb r3,[r11]
        b palabra2

 etiqueta3:
        add r1,#1
        strb r3,[r4]
        b palabra3
 etiqueta4:
        strb r3,[r12]
        b ya_remplace

  ya_remplace:
        bx lr


validarModo:
cmp r6,#99
bne validarModo2
bx lr

validarModo2:
cmp r6,#100
bne mostrarError33
bx lr

validarCorrimiento:

cmp r6,#48
bcc mostrarError2
cmp r6,#57
bhi mostrarError2
bx lr

validarChar:
cmp r6,#0x20     @compara con espacio
beq volverChar
cmp r6,#122    @mayor a z minuscula
bhi mostrarError
cmp r6,#65
bcc mostrarError @menor a A mayuscula tira error directamente.
cmp r6,#90   @si es mayor a Z mayuscula
bhi checkChar
bx lr


checkChar:
cmp r6,#97       @menor a a minuscula
bcc mostrarError
bx lr

volverChar:
bx lr

mostrarError:

ldr r1,=msjInterrupt
ldr r2,=msjInterruptLength
bl presentacion
b options

mostrarError2:

ldr r1,=msjInterrupt2
ldr r2,=msjInterrupt2Length
bl presentacion
b options

mostrarError33:

ldr r1,=msjInterrupt3
ldr r2,=msjInterrupt3Length
bl presentacion
b options





.fnend
@----------------------------------------------------------
convertir_ascii_a_entero:
    .fnstart
    push {lr}
        eor r0, r0
        eor r6, r6
        eor r5, r5
        eor r10, r10
        mov r10, #10
comparo_init:
        ldrb r2, [r1]
        cmp r2, #0
        beq fin
        cmp r2, #0
        bne comparo_cont


comparo_cont:
        add r6, #1
        cmp r6, #2
        beq mult

comparo_num:
        cmp r2, #'9'
        bls comparo_num2

comparo_num2:
        cmp r2, #0x30
        bhi convierto

convierto:
        sub r2, #0x30
        add r0, r2
        add r5,r5,r0
        add r1, #1
        eor r0,r0
        bal comparo_init
mult:
        mul r5, r10
        bal comparo_num

fin:
        cmp r5,#27
        bhi mostrarError2
        pop {lr}
        bx lr

        .fnend
@----------------------------------------------------------
extraer_opcion:
    .fnstart
    push {lr}
     eor r0, r0
    ldr r1, =modoExtraido
comparo_op:
    ldrb r2, [r1]
    cmp r2, #0x30
    beq fin
    cmp r2, #0x63
    beq codificar

    cmp r2, #0x64
    beq decodificar

  /*Aca podria ir un print para indicar que se ingreso otra cosa que c o d, o directamente tirar un error */

fin_op:
    bx lr
    .fnend

@-----------------------------------------------------------
codificar:
   .fnstart
        eor r9,r9
         eor r3,r3
        ldr r1, =mensajeExtraido
    etiqueta_c:
       ldrb r2,[r1]
       cmp r2,#0 /*termino la frase*/

       beq  ya_remplace_c
        cmp r2,#' '/*pregunto si es un espacio*/
        beq etiqueta2_c @si es not equal, se codifica
        add r3,r2,r5   @cargo caracter pero desplazado
       bl comparar
       strb r3,[r1]  @guardo en memoria el caracter
      b etiqueta2_c

comparar:

cmp r2,#97  @caracter es mayor o igual a a minuscula
bcs point1
cmp r2,#90 @caracter es menor o igual a Z mayuscula
ble point3

point3:

cmp r3,#90
bhi calcular_Borde_Mayus
bx lr


calcular_Borde_Mayus:

 eor r8,r8
   sub r8,r3,#90 @r8 tiene lo que me pasé de z
   sub r9,r5,r8   @calculo cuanto puedo recorrer hasta la z
   mov r3,#0
   add r3,r2,r9  @cargo el caracter pero desplazado
   mov r6,#64
  @ sub r8, #1
   add r6,r6,r8
   strb r6,[r1]
   b etiqueta2_c


point1:

cmp r3,#122
bhi calcular_Borde
bx lr


calcular_Borde:
  eor r8,r8
   sub r8,r3,#122 @r8 tiene lo que me pasé de z
   sub r9,r5,r8   @calculo cuanto puedo recorrer hasta la z
   mov r3,#0
   add r3,r2,r9  @cargo el caracter pero desplazado
   mov r6,#96
  @ sub r8, #1
   add r6,r6,r8
   strb r6,[r1]

  etiqueta2_c:
        add r1,#1
        b etiqueta_c
  ya_remplace_c:
        mov r0, #0x20
        eor r2, r2
        ldrb r2, [r4]
        strb r0, [r1], #1
        str r2, [r1], #1
        pop {lr}
        bx lr

.fnend
@-----------------------------------------------------------

@-----------------------------------------------------------
decodificar:
   .fnstart

        eor r3,r3
        ldr r1, =mensajeExtraido
    etiqueta_d:
       ldrb r2,[r1]
       cmp r2,#0 /*termino la frase*/

       beq  ya_remplace_d
        cmp r2,#' '/*pregunto si es un espacio*/
        beq etiqueta2_d @si es not equal, se codifica
        sub r3,r2,r5   @cargo caracter pero desplazado
        bl calculo
        strb r3,[r1]  @guardo en memoria el caracter
        b etiqueta2_d

calculo:
cmp r2,#97  @caracter es mayor o igual a a minuscula
bcs check1
cmp r2,#90 @caracter es menor o igual a Z mayuscula
ble check3

check3:

cmp r2,#65  @caracter es mayor o igual a A mayuscula
bcs check4

check4:
cmp r3,#65  @compara el valor corrido con A mayuscula para ver si se pasó
bcc calcular_Borde_Inf_Mayus @calcula corrimiento hacia atras.
bx lr


check1:

cmp r2,#122
ble check2

check2:

cmp r3,#97
bcc calcular_Borde_Inf
bx lr


calcular_Borde_Inf_Mayus:

   eor r8,r8
   mov r12,#65
   sub r8,r12,r3 @r8 tiene lo que me pasé de a
   sub r9,r5,r8   @calculo cuanto puedo recorrer hasta la a
   mov r3,#0
   sub r3,r2,r9  @cargo el caracter pero desplazado
   mov r6,#90
  @ sub r8, #1
   sub r6,r6,r8
   add r6,r6,#1
   strb r6,[r1]
   b etiqueta2_d


calcular_Borde_Inf:

   eor r8,r8
   mov r12,#97
   sub r8,r12,r3 @r8 tiene lo que me pasé de a
   sub r9,r5,r8   @calculo cuanto puedo recorrer hasta la a
   mov r3,#0
   sub r3,r2,r9  @cargo el caracter pero desplazado
   mov r6,#122
  @ sub r8, #1
   sub r6,r6,r8
   add r6,r6,#1
   strb r6,[r1]
   b etiqueta2_d

  etiqueta2_d:
        add r1,#1
        b etiqueta_d
  ya_remplace_d:
        pop {lr}
        bx lr

.fnend
@-----------------------------------------------------------
convertir_entero_a_ascii: @ARREGLAR COMPARADOR NORMAL Y EN PALABRA 1 ANTES DE GUARDAR
      .fnstart
      push {lr}
      eor r0, r0
      eor r3, r3
      mov r3, r9
      ldr r4, =caracteres_procesados
      cmp r9, #10
      bcs extraer_digitos
comparo_ch:
        cmp r9,#0
        beq fin_ch
        cmp r9, #9
        bls comparo2_ch
        add r0, #1
        b comparo_ch

comparo2_ch:
        cmp r9, #0
        bhi convierto_ch

convierto_ch:
        add r9, #0x30
        add r0, r9
        strb r0, [r4]
fin_ch:
        pop {lr}
        bx lr
        .fnend
@-----------------------------------------------------------
extraer_digitos: @DIVIDE DIGITOS DE 2 CIFRAS
.fnstart
    mov r0, r9
    mov r1, r0
    mov r6, #10
    bl division

.fnend

division:
.fnstart
eor r0, r0

ciclo:
    cmp r1, r6
    bcc finCiclo
    sub r1, r6
    add r0, #1
    b ciclo
finCiclo:
 mov r2,r1  @R2 = Resto
 mov r1, r0 @R1 = A/B
 bal extraer_digitos_2

   .fnend
@-------------------------------------------------------
extraer_digitos_2:
.fnstart
eor r0, r0
comparo_ch2:
        cmp r1,#0
        beq fin_ch2
        cmp r1, #9
        bls comparo2_ch2
        add r0, #1
        b comparo_ch2

comparo2_ch2:
        cmp r1, #0
        bhi convierto_ch2

convierto_ch2:
        add r1, #0x30
        add r0, r1
        strb r0, [r4]
    @-----OP PARA EL SEGUNDO DIGITO
eor r0, r0
comparo_ch2op2:
        cmp r2, #9
        bls comparo2_ch2op2
        add r0, #1
        b comparo_ch2op2

comparo2_ch2op2:
        cmp r2, #0
        bhi convierto_ch2op2

convierto_ch2op2:
        add r2, #0x30
        add r0, r2
        strb r0, [r4, #1]
fin_ch2:
        pop {lr}

        bx lr

    .fnend
@-----------------------------------------------------------
bit_paridad:
        .fnstart
        eor r0, r0
        eor r2, r2

        eor r4, r4
        ldr r4, =paridad
par_ciclo:
        sub r3, #2 @Resto 2 a r9 por ciclo
        cmp r3, #0 @si termina en 0 es par
        beq guardar_cero

        cmp r3, #1 @si termina en 1 es impar
        beq guardar_uno

        bal par_ciclo

guardar_cero:
        mov r0, #0x31
        strb r0, [r4]
        bx lr
guardar_uno:
        mov r0, #0x30
        strb r0, [r4]
        bx lr


        .fnend
@-----------------------------------------------------------
.global cesar1
cesar1:

        eor r0, r0
        eor r1, r1
        eor r2, r2
        eor r3, r3
        eor r4, r4
        eor r5, r5
        eor r6, r6
        eor r8, r8
        eor r9, r9
        eor r10, r10
        eor r11, r11



        ldr r1,=indicacionesUsoCesar1
        mov r2,#86
        bl presentacion
        ldr r1,=salto
        mov r2,#2
        bl presentacion
        ldr r1,=formatoCesar1
        mov r2,#107
        bl presentacion
        ldr r1,=salto
        mov r2,#2
        bl presentacion
        ldr r1,=ejemploCesar1
        ldr r2,=ejemploCesar1Length
        bl presentacion
        ldr r1,=salto
        mov r2,#2
        bl presentacion


    @leemos la tecla
    bl leerMensajeCesar1

    bl remplazo

    ldr r1, =claveExtraida
    bl convertir_ascii_a_entero
    bl convertir_entero_a_ascii
    bl bit_paridad
    bl extraer_opcion
    ldr r1, =mensajeExtraido
   /* bl codificar */



      @Imprimir letra ingresada"

    ldr r1,=outputResultadoCesar1
    mov r2,#22
    bl presentacion


    ldr r2, =longitudmensajeExtraido @ Leer # caracteres
    ldr r1, =mensajeExtraido    @Cargamos en r1 la direccion del mensaje


    bl imprimirString
      ldr r1,=salto
    mov r2,#2
    bl presentacion

   @bl convertir_entero_a_ascii

    ldr r1,=outputCantidadCaracteresCesar1
    mov r2,#35
    bl presentacion

    ldr r1, =caracteres_procesados
    ldr r2, =longitudcaracteres
    bl presentacion


    bl newLine    @ Para acomodar el prompt al terminar el programa

    b options


@############################################################################################ CESAR 1 FIN #####################################################


@############################################################################################# CESAR 2 INICIO  ##################################################
.global cesar2


cesar2:


ldr r1,=instruccionesCesar2
ldr r2,=instruccionesCesar2Length
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=ejemploCesar2
ldr r2,=ejemploCesar2Length
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion


@----------------------------------------------------------
leerMensajeCesar2:
.fnstart

    mov r7, #3    @ Lectura x teclado
    mov r0, #0      @ Ingreso de cadena

    ldr r2, =longitudInputUsuario @ Leer # caracteres
    ldr r1, =ingresoUsuarioInput @ donde se guarda la cadena ingresada
    ldr r11, =mensajeEncriptado
    ldr r4, =pistaExtraida

    swi 0        @ SWI, Software interrup


.fnend
@----------------------------------------------------------

mov r10,#1


remplazoCesar2:
.fnstart
        mov r3,#0
        eor r9,r9
        eor r10,r10
        eor r8,r8
        mov r2,#0


  palabra1Cesar2:
       ldrb r6,[r1] @@cargo el byte de la posicion 1, valor guardado en r8
       ldrb r5,[r11]
       cmp r6,#0 @/termino la frase/
       beq  ya_remplaceCesar2
        cmp r6,#';'
        beq etiqueta2Cesar2 @@si es not equal, se codifica

        strb r6,[r11]  @@guardo en r3 el byte
        add r1,#1
        add r11,#1
        b palabra1Cesar2

  palabra2Cesar2:
       ldrb r6,[r1] @@cargo el byte de la posicion 1, valor guardado en r8
       ldrb r2,[r4]
       cmp r6,#0 @/termino la frase/
       beq  ya_remplaceCesar2
        cmp r6,#';'
        beq etiqueta3Cesar2 @@si es not equal, se codifica

        strb r6,[r4]  @@guardo en r3 el byte
        add r1,#1
        add r4,#1
        b palabra2Cesar2

  etiqueta2Cesar2:
        add r1,#1
        strb r3,[r11]
        b palabra2Cesar2

 etiqueta3Cesar2:

        strb r3,[r4]
        b ya_remplaceCesar2

  ya_remplaceCesar2:


.fnend
@----------------------------------------------------------


b decodificarCesar2
ldr r1,=salto
mov r2,#2
bl presentacion


fuerzaCesar2:

ldrb r3,[r1] @puntero texto
ldrb r4,[r2] @puntero pista, primer caracter.

cmp r3,#0    @significa que recorri toda la cadena encriptada
beq reDecodificarCesar2  @se tendrá que redecodificar con un valor de corrimiento mas
cmp r3,r4
beq compararCesar2 @inicia comparacion.
add r1,#1     @incrementa puntero de encriptado
b fuerzaCesar2
compararCesar2:
ldrb r3,[r1]
ldrb r4,[r2]

cmp r4,#0
beq encontroCorrimientoAdecuadoCesar2
cmp r4,r3
bne limpiarCesar2
add r2,#1    @incremento puntero de palabra pista
add r1,#1
b compararCesar2

limpiarCesar2:

ldr r2,=pistaExtraida
b fuerzaCesar2

printFinal:

ldr r1,=outputCesar2
mov r2,#24
bl presentacion

ldr r1,=mensajeEncriptado
ldr r2,=longitudmensajeExtraido
bl presentacion

ldr r1,=salto
mov r2,#2
bl presentacion

ldr r1,=outputValorCorrimiento
ldr r2,=outputValorCorrimientolength
bl presentacion

ldr r1,=caracteres_procesados
ldr r2,=longitudcaracteres
bl presentacion

b options


reDecodificarCesar2:

@esta funcion debe decodificar el mensaje encriptado original, con otro valor de corrimiento
@hago este mov solo para probar que pase algo
b codificarCesar2

encontroCorrimientoAdecuadoCesar2:
@debe asignar corrimiento actual al registro r0
mov r0,r10
ldr r4,=caracteres_procesados
mov r9,r0
bl convertir_entero_a_ascii
b printFinal


finCesar2:

decodificarCesar2:
   .fnstart
        eor r3,r3
        ldr r1, =mensajeEncriptado
    etiqueta_dCesar2:
       ldrb r2,[r1]
       cmp r2,#0 /*termino la frase*/

       beq  ya_remplace_dCesar2
        cmp r2,#' '/*pregunto si es un espacio*/
        beq etiqueta2_dCesar2 @si es not equal, se codifica
        sub r3,r2,r10   @cargo caracter pero desplazado
        bl calculoCesar2
        strb r3,[r1]  @guardo en memoria el caracter
        b etiqueta2_dCesar2

calculoCesar2:
cmp r2,#97  @caracter es mayor o igual a a minuscula
bcs check1Cesar2
cmp r2,#90 @caracter es menor o igual a Z mayuscula
ble check3Cesar2

check3Cesar2:

cmp r2,#65  @caracter es mayor o igual a A mayuscula
bcs check4Cesar2

check4Cesar2:
cmp r3,#65  @compara el valor corrido con A mayuscula para ver si se pasó
bcc calcular_Borde_Inf_MayusCesar2 @calcula corrimiento hacia atras.
bx lr


check1Cesar2:

cmp r2,#122
ble check2Cesar2

check2Cesar2:

cmp r3,#97
bcc calcular_Borde_InfCesar2
bx lr


calcular_Borde_Inf_MayusCesar2:

   eor r8,r8
   mov r12,#65
   sub r8,r12,r3 @r8 tiene lo que me pasé de a
   sub r9,r5,r8   @calculo cuanto puedo recorrer hasta la a
   mov r3,#0
   sub r3,r2,r9  @cargo el caracter pero desplazado
   mov r6,#90
  @ sub r8, #1
   sub r6,r6,r8
   add r6,r6,#1
   strb r6,[r1]
   b etiqueta2_dCesar2


calcular_Borde_InfCesar2:

   eor r8,r8
   mov r12,#97
   sub r8,r12,r3 @r8 tiene lo que me pasé de a
   sub r9,r5,r8   @calculo cuanto puedo recorrer hasta la a
   mov r3,#0
   sub r3,r2,r9  @cargo el caracter pero desplazado
   mov r6,#122
  @ sub r8, #1
   sub r6,r6,r8
   add r6,r6,#1
   strb r6,[r1]
   b etiqueta2_dCesar2

  etiqueta2_dCesar2:
        add r1,#1
        b etiqueta_dCesar2
  ya_remplace_dCesar2:
       ldr r1,=mensajeEncriptado
       ldr r2,=pistaExtraida
       b fuerzaCesar2

.fnend

codificarCesar2:
   .fnstart

         push {lr}
         eor r3,r3
        ldr r1, =mensajeEncriptado
       etiqueta_cCesar2:
       ldrb r2,[r1]
       cmp r2,#0 /*termino la frase*/
       beq  ya_remplace_cCesar2
       cmp r2,#' '/*pregunto si es un espacio*/
       beq etiqueta2_cCesar2 @si es not equal, se codifica
       add r3,r2,r10   @cargo caracter pero desplazado
       bl compararChar
       pop {lr}
       strb r3,[r1]  @guardo en memoria el caracter
       b etiqueta2_cCesar2

imprimirNoCoincidencias:
    ldr r1,=noCoincidencias
    ldr r2,=noCoincidenciasLength
    bl presentacion
    ldr r1,=salto
    mov r2,#2
    bl presentacion
    b options

compararChar:

cmp r2,#97  @caracter es mayor o igual a a minuscula
bcs point1Cesar2
cmp r2,#90 @caracter es menor o igual a Z mayuscula
ble point3Cesar2

point3Cesar2:

cmp r3,#90
bhi calcular_Borde_MayusCesar2
bx lr

calcular_Borde_MayusCesar2:

 eor r8,r8
   sub r8,r3,#90 @r8 tiene lo que me pasé de z
   sub r9,r10,r8   @calculo cuanto puedo recorrer hasta la z
   mov r3,#0
   add r3,r2,r9  @cargo el caracter pero desplazado
   mov r6,#64
  @ sub r8, #1
   add r6,r6,r8
   strb r6,[r1]
   b etiqueta2_cCesar2

point1Cesar2:

cmp r3,#122
bhi calcular_BordeCesar2
bx lr



calcular_BordeCesar2:
   eor r8,r8
   sub r8,r3,#122 @r8 tiene lo que me pasé de z
   sub r9,r10,r8   @calculo cuanto puedo recorrer hasta la z
   mov r3,#0
   add r3,r2,r9  @cargo el caracter pero desplazado
   mov r6,#96
  @ sub r8, #1
   add r6,r6,r8
   strb r6,[r1]

  etiqueta2_cCesar2:
        add r1,#1
        b etiqueta_cCesar2
  ya_remplace_cCesar2:
         add r10,#1
         cmp r10,#100  @no encontró ninguna coincidencia.
         beq imprimirNoCoincidencias
         b decodificarCesar2
.fnend
@-----------------------------------------------------------
@----------------------------------------------------------


@############################################################################################# CESAR 2 FIN     ##################################################

